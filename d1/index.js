/*
	placeholder API: /Users/coeljohncastro/Documents/CoelJohn-Castro/b190/s49/d1/index.html
*/

fetch('https://jsonplaceholder.typicode.com/posts').then((response)=>response.json()).then((data)=>showPosts(data));

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post)=>{
	postEntries += `
		<div id ="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost(${post.id})">Edit</button>
			<button onclick="deletePost(${post.id})">Delete</button>
		</div>
	`
	});
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// add post data
document.querySelector('#form-add-post').addEventListener('submit',(e)=>{
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts",{
		method:"POST",
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {'Content-type':'application/json; charset=UTF-8'}
	})
	.then((response)=>response.json())
	.then((data)=>{
		console.log(data);
		alert('Successfully added');

		// lets input fields to be blank after the alert is closed
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;

	})
})

// Edit Post
/*
	make the edit button work by creating editPost function
		the values received from the div, should be copied inside the input fields of the edit post form
			id
			title
			body
*/

const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
}

// Update Post
/*
	for function, select the edit post form through the use of document.querySelector, it should listen to submit event and will perform the following block of codes
		- set the method to PUT
		- send the body as stringified json with the following properties:
			- id = id input field of the edit form
			- title = title input field of the edit post form
			- body = body input field of the edit post form
			- userId = 1
		- set the headers for content type of application/json, charset-UTF-8
		- use then methods to convert the response into json and log it in the console
		- send an alert for sussful updating
		- empty the edit post form after the codes has been executed
*/

document.querySelector('#form-edit-post').addEventListener('submit', (e)=>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
			}),
			headers: {'Content-type':'application/json; charset=UTF-8'}
		})
		.then((response)=>response.json())
		.then((data)=>{
			console.log(data);
			alert('Successfully updated');

			document.querySelector(`#post-title-${data.id}`).innerHTML = data.title;
			document.querySelector(`#post-body-${data.id}`).innerHTML = data.body;

			document.querySelector('#txt-edit-id').value = null;
			document.querySelector('#txt-edit-title').value = null;
			document.querySelector('#txt-edit-body').value = null;
			// setAttribute is used to allow setting up or bring back the removed attribute of the HTML elements
			/*
				it is accepting 2 arguments
					1st - the string that identifies the attribute to be set
					2nd - boolean to state whethe the attribute is to be set (true); (false) is not accepted as the removing of the attribute; but removing the "true" would not work for the setAttribute
			*/
			document.querySelector('#btn-submit-update').setAttribute('disabled', true);
		})
	})

// s49 Activity - deleting a post

function deletePost(id) {
 fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
   method: 'DELETE',
 })
 .then((res) => {
   res.json()
 })
 .then((data) => data)
 	document.querySelector(`#post-title-${id}`).textContent = '';
 	document.querySelector(`#post-body-${id}`).textContent = '';
 	document.querySelector(`#post-${id}`).remove();
}